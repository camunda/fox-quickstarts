package com.camunda.fox.quickstart.cockpit.router;

import static org.junit.Assert.assertEquals;

import java.util.HashMap;

import javax.inject.Inject;

import org.activiti.engine.HistoryService;
import org.activiti.engine.RuntimeService;
import org.activiti.engine.runtime.ProcessInstance;
import org.jboss.arquillian.container.test.api.Deployment;
import org.jboss.arquillian.junit.Arquillian;
import org.jboss.shrinkwrap.api.ShrinkWrap;
import org.jboss.shrinkwrap.api.spec.JavaArchive;
import org.jboss.shrinkwrap.api.spec.WebArchive;
import org.jboss.shrinkwrap.resolver.api.DependencyResolvers;
import org.jboss.shrinkwrap.resolver.api.maven.MavenDependencyResolver;
import org.junit.Test;
import org.junit.runner.RunWith;

@RunWith(Arquillian.class)
public class ArquillianTestCase {

  @Deployment
  public static WebArchive createDeployment() {
    MavenDependencyResolver resolver = DependencyResolvers.use(MavenDependencyResolver.class)
      .goOffline()
      .loadMetadataFromPom("pom.xml");

    return ShrinkWrap
            .create(WebArchive.class, "cockpit-router-configuration.war")
            // prepare as process application archive for fox platform
            .addAsLibraries(resolver.artifact("com.camunda.fox.platform:fox-platform-client").resolveAs(JavaArchive.class))
            .addAsWebResource("META-INF/processes.xml", "WEB-INF/classes/META-INF/processes.xml")
            .addAsWebResource("META-INF/beans.xml", "WEB-INF/classes/META-INF/beans.xml")
            // add your own classes (could be done one by one as well)
            .addPackages(true, "com.camunda.fox.quickstart.cockpit")
            // add process definition
            .addAsResource("ConfigureRouter.bpmn");
  }

  @Inject
  private RuntimeService runtimeService;

  @Inject
  private HistoryService historyService;

  @Test
  public void testHappyPath() throws InterruptedException {
    HashMap<String, Object> variables = new HashMap<String, Object>();
    variables.put("serialnumber", "23");
    ProcessInstance processInstance = runtimeService.startProcessInstanceByKey("configure-router", variables);

    // TODO : Improve this!
    Thread.sleep(2000);

    assertEquals("process instance didn't end automatically in happy path", //
            1, historyService.createHistoricProcessInstanceQuery().processInstanceId(processInstance.getId()).finished().count());
  }
}
