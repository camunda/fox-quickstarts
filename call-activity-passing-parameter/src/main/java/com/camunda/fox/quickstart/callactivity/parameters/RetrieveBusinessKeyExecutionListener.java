package com.camunda.fox.quickstart.callactivity.parameters;

import org.activiti.engine.delegate.DelegateExecution;
import org.activiti.engine.delegate.ExecutionListener;
import org.activiti.engine.impl.persistence.entity.ExecutionEntity;

/**
 * Attach this Execution Listener to the process start event of a process model
 * in order to set the business key to the same value as the super process
 * (if it is called)
 * 
 * @author ruecker
 */
public class RetrieveBusinessKeyExecutionListener implements ExecutionListener {

  @Override
  public void notify(DelegateExecution execution) throws Exception {
    ((ExecutionEntity)execution).getProcessInstance().setBusinessKey(
            ((ExecutionEntity)execution).getSuperExecution().getProcessBusinessKey());
  }

}
