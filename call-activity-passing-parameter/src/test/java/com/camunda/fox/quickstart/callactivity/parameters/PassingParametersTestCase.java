package com.camunda.fox.quickstart.callactivity.parameters;

import java.util.List;
import java.util.Map;

import org.activiti.engine.test.ActivitiTestCase;
import org.activiti.engine.test.Deployment;

public class PassingParametersTestCase extends ActivitiTestCase {

  @Deployment(resources = "SuperProcess.bpmn")
  public void testParsingAndDeployment() {
  }
  
  @Deployment(resources = "SuperProcess.bpmn")
  public void testNoStartEvent() {
    runtimeService.startProcessInstanceByKey("super-process", "0815");
    
    List<Map<String, String>> collectedValues = CollectVariableValuesDelegate.values;

    // we have the variable states at 3 collection points:
    assertEquals(2, collectedValues.size());
    
    Map<String, String> variablesInSubProcess = collectedValues.get(0);
    assertEquals("andreas", variablesInSubProcess.get("x"));
    assertEquals("christian", variablesInSubProcess.get("y"));
    assertEquals("0815", variablesInSubProcess.get(CollectVariableValuesDelegate.BUSINESS_KEY_NAME));

    Map<String, String> variablesInSuperProcessAfterSubProcess = collectedValues.get(1);
    assertEquals("kristin", variablesInSuperProcessAfterSubProcess.get("x"));
    assertEquals("robert", variablesInSuperProcessAfterSubProcess.get("y"));
    assertEquals("0815", variablesInSuperProcessAfterSubProcess.get(CollectVariableValuesDelegate.BUSINESS_KEY_NAME));
  }
  
}