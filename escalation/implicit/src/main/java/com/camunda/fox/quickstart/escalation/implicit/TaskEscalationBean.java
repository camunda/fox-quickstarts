package com.camunda.fox.quickstart.escalation.implicit;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import javax.enterprise.context.ConversationScoped;
import javax.faces.bean.ViewScoped;
import javax.inject.Inject;
import javax.inject.Named;

import org.activiti.engine.TaskService;
import org.activiti.engine.task.Task;

@Named
@ConversationScoped
public class TaskEscalationBean implements Serializable {

  private static final long serialVersionUID = 1L;

  @Inject
  private TaskService taskService;

  private Date date = new Date();

  private String escalateToUser = "kermit";

  public List<Task> getDueTasks() {
    List<Task> list = taskService.createTaskQuery().dueBefore(date).list();
    return list;
  }

  public void escalateTask(Task task) {
    taskService.setAssignee(task.getId(), escalateToUser);
  }

  public void escalateTasks() {
    for (Task task : getDueTasks()) {
      escalateTask(task);
    }
  }

  public Date getDate() {
    return date;
  }

  public void setDate(Date date) {
    this.date = date;
  }

  public String getEscalateToUser() {
    return escalateToUser;
  }

  public void setEscalateToUser(String escalateToUser) {
    this.escalateToUser = escalateToUser;
  }

}
