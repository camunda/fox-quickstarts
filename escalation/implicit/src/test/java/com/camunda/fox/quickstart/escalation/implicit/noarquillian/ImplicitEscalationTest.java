package com.camunda.fox.quickstart.escalation.implicit.noarquillian;

import org.activiti.engine.ProcessEngine;
import org.activiti.engine.test.ActivitiRule;
import org.activiti.engine.test.Deployment;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;

import com.camunda.fox.quickstart.escalation.implicit.ImplicitEscalationTestBase;

/**
 * Test case running standalone leveraging only the fox engine
 * @author nico.rehwaldt
 */
public class ImplicitEscalationTest extends ImplicitEscalationTestBase {
  
  @Rule
  public ActivitiRule activitiRule = new ActivitiRule();
  
  private ProcessEngine engine;
  
  @Before
  public void before() {
    engine = activitiRule.getProcessEngine();
  }
  
  @Test
  @Deployment(resources="escalation-implicit.bpmn")
  public void shouldParseExtension() {
    
  }
  
  @Test
  @Deployment(resources="escalation-implicit.bpmn")
  public void shouldAssignDueDateToTaskWithExtension() {
    shouldHandleEscalationInProcessCorrectly(engine);
  }
}
