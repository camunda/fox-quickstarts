package com.camunda.fox.quickstart.escalation.explicit.noarquillian;

import org.activiti.engine.ProcessEngine;
import org.activiti.engine.impl.ProcessEngineImpl;
import org.activiti.engine.impl.jobexecutor.JobExecutor;
import org.activiti.engine.test.ActivitiRule;
import org.activiti.engine.test.Deployment;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;

import com.camunda.fox.quickstart.escalation.explicit.ExplicitEscalationTestBase;


/**
 * Test case running standalone leveraging only the fox engine
 * @author nico.rehwaldt
 */
public class ExplicitEscalationTest extends ExplicitEscalationTestBase {

  @Rule
  public ActivitiRule activitiRule = new ActivitiRule();
  
  private ProcessEngine engine;
  private JobExecutor jobExecutor;
  
  @Before
  public void before() {
    this.engine = activitiRule.getProcessEngine();
    
    this.jobExecutor = ((ProcessEngineImpl) activitiRule.getProcessEngine()).getProcessEngineConfiguration().getJobExecutor();
  }
  
  @Test
  @Deployment(resources="escalation-explicit.bpmn")
  public void shouldHandleExplicitEscalation() throws Exception {
    
    jobExecutor.start();
    
    shouldHandleEscalationCorrectly(engine);
  }
}
