package com.camunda.fox.bpmn2.util;

import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author nico.rehwaldt
 */
public class Filter {

  public static <T> List<T> byType(Class<T> cls, List<?> elements) {
    ArrayList<T> results = new ArrayList<T>();
    for (Object o: elements) {
      if (cls.isInstance(o)) {
        results.add(cls.cast(o));
      }
    }
    
    return results;
  }
}
