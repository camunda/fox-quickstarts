package com.camunda.fox.quickstart.test.mock;

import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.util.HashMap;

import org.activiti.engine.runtime.Job;
import org.activiti.engine.runtime.ProcessInstance;
import org.activiti.engine.test.ActivitiTestCase;
import org.activiti.engine.test.Deployment;
import org.activiti.engine.test.mock.Mocks;

import com.camunda.fox.quickstart.bean.HelloBean;

public class JUnit3ProcessTest extends ActivitiTestCase {

  @Deployment(resources = "process.bpmn")
  public void testHappyPath() throws InterruptedException {
    // Initialize mocks
    HelloBean helloBean = mock(HelloBean.class);
    when(helloBean.sayHello("John")).thenReturn("Hello John!");
    // Register mocks
    Mocks.register("helloBean", helloBean);

    // Start process instance with variables
    HashMap<String, Object> variables = new HashMap<String, Object>();
    variables.put("name", "John");
    ProcessInstance processInstance = runtimeService.startProcessInstanceByKey("hello_process", variables);

    // Print the ID of the process instance that has been started to the console
    System.out.println("Started process instance id " + processInstance.getId());

    // un-comment the code below if you make the service task in hellp_process asynchronous!
    
//    // assert the asynchronous flag was set on the service task
//    Job job = managementService.createJobQuery().singleResult();
//    assertNotNull(job);
//    
//    // manually execute the job
//    managementService.executeJob(job.getId());
    
    assertProcessEnded(processInstance.getId());

    // verify the service was called exactly one time with the parameter "John" 
    verify(helloBean, times(1)).sayHello("John");

    // reset the mocks to make sure we have a clean start in the next test case
    Mocks.reset();
  }
}
