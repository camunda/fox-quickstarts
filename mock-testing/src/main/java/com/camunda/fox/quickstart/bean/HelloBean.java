package com.camunda.fox.quickstart.bean;

public class HelloBean {

	public String sayHello(String name) {
		return "Hello " + name + "!";
	}
	
}
