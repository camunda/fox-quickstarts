package com.camunda.fox.quickstart.usertask.foureyes.simple.noarquillian;

import org.activiti.engine.test.ActivitiTestCase;
import org.activiti.engine.test.Deployment;

/**
 * Test case starting an in-memory database backed Process Engine just to test
 * if the process definition is deployable.
 * 
 * @author ruecker
 */
public class ParsingTestCase extends ActivitiTestCase {

  @Deployment(resources = "FourEyesSimple.bpmn")
  public void testParsingAndDeployment() {
  }

}