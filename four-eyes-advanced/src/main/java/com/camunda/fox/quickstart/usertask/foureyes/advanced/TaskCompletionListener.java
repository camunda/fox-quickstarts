package com.camunda.fox.quickstart.usertask.foureyes.advanced;

import java.io.InputStream;

import javax.inject.Named;

import org.activiti.cdi.impl.util.ProgrammaticBeanLookup;
import org.activiti.engine.ProcessEngine;
import org.activiti.engine.delegate.DelegateTask;
import org.activiti.engine.delegate.TaskListener;
import org.activiti.engine.impl.bpmn.parser.BpmnParseListener;
import org.activiti.engine.impl.persistence.entity.TaskEntity;
import org.w3c.dom.Element;


/**
 * Listener to save user who completed the task as process variable. Could be added via an 
 * {@link BpmnParseListener}, then you even don't have to add it manually to the XML.
 * 
 * @author ruecker
 */
@Named
public class TaskCompletionListener implements TaskListener {
  
  @Override
  public void notify(DelegateTask task) {    
    // With the FourEyesExtensionsParseListener we could do something like:
    // String groupName =  (String) ((TaskEntity) task).getExecution().getActivity().getProperties().get(Helper.FOUR_EYES_GROUP_NAME);

    // Without we fall back to XML parsing:
    InputStream processModelInputStream = ProgrammaticBeanLookup.lookup(ProcessEngine.class).getRepositoryService().getProcessModel(task.getProcessDefinitionId());
    Element fourEyeExtensionElement = Helper.getUserTaskExtensions(processModelInputStream, ((TaskEntity) task).getExecution().getActivityId(), Helper.FOUR_EYES_GROUP_NAME);

    String variableName = Helper.getVariableName(fourEyeExtensionElement.getAttribute("name"));
    
    // We cannot use a CDI Bean as TaskListener until http://jira.codehaus.org/browse/ACT-1065 is resolved
    // so we do a programatic lookup from within a normal task listener created by the engine
    AuthenticationService authenticationService = ProgrammaticBeanLookup.lookup(AuthenticationService.class);    
    
    String loggedInUser = authenticationService.getLoggedInUser();
    
    // chek that this user has not done the last task
    String lastUser = (String) task.getExecution().getVariable(variableName);
    if (lastUser!=null && lastUser.equals(loggedInUser)) {
      throw new RuntimeException("Same user has already completed the first task, cannot complete second task, violates 4-eyes-principle");
    }

    if (task.getAssignee()==null || !task.getAssignee().equals(loggedInUser)) {
      // task was not assigned/claimed before completion, fix this.
      task.setAssignee(loggedInUser);
    }    
    
    // and write a process variable for it
    task.getExecution().setVariable(variableName, loggedInUser);
  }

}
