# Introduction
The camunda fox quickstarts show you how to implement specific requirements for the camunda fox BPM platform. 
The quickstarts normally run on the [camunda fox community edition](http://www.camunda.com/fox/community/download/) or enterprise edition on all supported application servers.

# Possible Restrictions
Some quickstarts may have specific restrictions on the environment, then you will find a note in the readme.txt of the quickstart.

# Overview of camunda fox demos
The quickstarts are one type of demos we do for the camunda fox BPM platform, please check the [camunda fox userguide](https://app.camunda.com/confluence/display/foxUserGuide/Examples+and+Tutorials) for details on this.

# Running the quickstarts
[Download your desired tag](https://bitbucket.org/camunda/fox-quickstarts/downloads) listed under the tab "Tags".
For more information see [camunda fox userguide](https://app.camunda.com/confluence/display/foxUserGuide/Download+and+run+examples)